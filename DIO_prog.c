/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          February 7th, 2019                  */
/*   Version :          V03                                 */
/*                                                          */
/* ******************************************************** */

#include "BIT_CALC.h"
#include "STD_TYPES.h"
//#include "DELAY.h"

#include "DIO_interface.h"
#include "DIO_private.h"
#include "DIO_config.h"


void DIO_voidInit(void)
{

	DIO_u8_DDRA = CONC_8BIT(	DIO_u8_PIN7_INIT_DIR,
								DIO_u8_PIN6_INIT_DIR,
								DIO_u8_PIN5_INIT_DIR,
								DIO_u8_PIN4_INIT_DIR,
								DIO_u8_PIN3_INIT_DIR,
								DIO_u8_PIN2_INIT_DIR,
								DIO_u8_PIN1_INIT_DIR,
								DIO_u8_PIN0_INIT_DIR);

	DIO_u8_DDRB = CONC_8BIT(	DIO_u8_PIN15_INIT_DIR,
								DIO_u8_PIN14_INIT_DIR,
								DIO_u8_PIN13_INIT_DIR,
								DIO_u8_PIN12_INIT_DIR,
								DIO_u8_PIN11_INIT_DIR,
								DIO_u8_PIN10_INIT_DIR,
								DIO_u8_PIN9_INIT_DIR,
								DIO_u8_PIN8_INIT_DIR);

	DIO_u8_DDRC = CONC_8BIT(	DIO_u8_PIN23_INIT_DIR,
								DIO_u8_PIN22_INIT_DIR,
								DIO_u8_PIN21_INIT_DIR,
								DIO_u8_PIN20_INIT_DIR,
								DIO_u8_PIN19_INIT_DIR,
								DIO_u8_PIN18_INIT_DIR,
								DIO_u8_PIN17_INIT_DIR,
								DIO_u8_PIN16_INIT_DIR);

	DIO_u8_DDRD = CONC_8BIT(	DIO_u8_PIN31_INIT_DIR,
								DIO_u8_PIN30_INIT_DIR,
								DIO_u8_PIN29_INIT_DIR,
								DIO_u8_PIN28_INIT_DIR,
								DIO_u8_PIN27_INIT_DIR,
								DIO_u8_PIN26_INIT_DIR,
								DIO_u8_PIN25_INIT_DIR,
								DIO_u8_PIN24_INIT_DIR);



	DIO_u8_PORTA = CONC_8BIT(	DIO_u8_PIN7_INIT_VAL,
								DIO_u8_PIN6_INIT_VAL,
								DIO_u8_PIN5_INIT_VAL,
								DIO_u8_PIN4_INIT_VAL,
								DIO_u8_PIN3_INIT_VAL,
								DIO_u8_PIN2_INIT_VAL,
								DIO_u8_PIN1_INIT_VAL,
								DIO_u8_PIN0_INIT_VAL);

	DIO_u8_PORTB = CONC_8BIT(	DIO_u8_PIN15_INIT_VAL,
								DIO_u8_PIN14_INIT_VAL,
								DIO_u8_PIN13_INIT_VAL,
								DIO_u8_PIN12_INIT_VAL,
								DIO_u8_PIN11_INIT_VAL,
								DIO_u8_PIN10_INIT_VAL,
								DIO_u8_PIN9_INIT_VAL,
								DIO_u8_PIN8_INIT_VAL);

	DIO_u8_PORTC = CONC_8BIT(	DIO_u8_PIN23_INIT_VAL,
								DIO_u8_PIN22_INIT_VAL,
								DIO_u8_PIN21_INIT_VAL,
								DIO_u8_PIN20_INIT_VAL,
								DIO_u8_PIN19_INIT_VAL,
								DIO_u8_PIN18_INIT_VAL,
								DIO_u8_PIN17_INIT_VAL,
								DIO_u8_PIN16_INIT_VAL);

	DIO_u8_PORTD = CONC_8BIT(	DIO_u8_PIN31_INIT_VAL,
								DIO_u8_PIN30_INIT_VAL,
								DIO_u8_PIN29_INIT_VAL,
								DIO_u8_PIN28_INIT_VAL,
								DIO_u8_PIN27_INIT_VAL,
								DIO_u8_PIN26_INIT_VAL,
								DIO_u8_PIN25_INIT_VAL,
								DIO_u8_PIN24_INIT_VAL);
	return;
}

/* Description: Set pin Direction
 * Inputs:
 * Outputs:
 *
 * */

u8 DIO_u8SetPinDirection(u8 Copy_u8PinNum, u8 Copy_u8PinDirection)
{
	u8 Local_u8Error;

	if(Copy_u8PinNum < DIO_u8_MAX_PIN_NUM)
	{
		switch(Copy_u8PinDirection)
		{
			case DIO_u8_PIN_INPUT:

				if((Copy_u8PinNum >= DIO_u8_PIN0 &&
					Copy_u8PinNum <= DIO_u8_PIN7))
				{
					CLR_BIT(DIO_u8_DDRA,Copy_u8PinNum);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN8 &&
						 Copy_u8PinNum <= DIO_u8_PIN15))
				{
					CLR_BIT(DIO_u8_DDRB,Copy_u8PinNum-EIGHT);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN16 &&
						 Copy_u8PinNum <= DIO_u8_PIN23))
				{
					CLR_BIT(DIO_u8_DDRC,Copy_u8PinNum-SIXTEEN);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN23 &&
						 Copy_u8PinNum <= DIO_u8_PIN31))
				{
					CLR_BIT(DIO_u8_DDRD,Copy_u8PinNum-TWENTY_FOUR);
				}
				break;


			case DIO_u8_PIN_OUTPUT:

				if((Copy_u8PinNum >= DIO_u8_PIN0 &&
					Copy_u8PinNum <= DIO_u8_PIN7))
				{
					SET_BIT(DIO_u8_DDRA,Copy_u8PinNum);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN8 &&
						 Copy_u8PinNum <= DIO_u8_PIN15))
				{
					SET_BIT(DIO_u8_DDRB,Copy_u8PinNum-EIGHT);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN16 &&
						 Copy_u8PinNum <= DIO_u8_PIN23))
				{
					SET_BIT(DIO_u8_DDRC,Copy_u8PinNum-SIXTEEN);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN23 &&
						 Copy_u8PinNum <= DIO_u8_PIN31))
				{
					SET_BIT(DIO_u8_DDRD,Copy_u8PinNum-TWENTY_FOUR);
				}
				break;

			default:
				Local_u8Error = STD_u8_ERROR;
				break;


		}
	}
	else
	{
		Local_u8Error = STD_u8_ERROR;
	}

	return Local_u8Error;

}

u8 DIO_u8SetPinValue(u8 Copy_u8PinNum, u8 Copy_u8PinValue)
{
	u8 Local_u8Error;

	if(Copy_u8PinNum < DIO_u8_MAX_PIN_NUM)
	{
		switch(Copy_u8PinValue)
		{
			case DIO_u8_PIN_LOW:

				if((Copy_u8PinNum >= DIO_u8_PIN0 &&
					Copy_u8PinNum <= DIO_u8_PIN7))
				{
					CLR_BIT(DIO_u8_PORTA,Copy_u8PinNum);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN8 &&
						 Copy_u8PinNum <= DIO_u8_PIN15))
				{
					CLR_BIT(DIO_u8_PORTB,Copy_u8PinNum-EIGHT);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN16 &&
						 Copy_u8PinNum <= DIO_u8_PIN23))
				{
					CLR_BIT(DIO_u8_PORTC,Copy_u8PinNum-SIXTEEN);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN23 &&
						 Copy_u8PinNum <= DIO_u8_PIN31))
				{
					CLR_BIT(DIO_u8_PORTD,Copy_u8PinNum-TWENTY_FOUR);
				}
				break;


			case DIO_u8_PIN_HIGH:

				if((Copy_u8PinNum >= DIO_u8_PIN0 &&
					Copy_u8PinNum <= DIO_u8_PIN7))
				{
					SET_BIT(DIO_u8_PORTA,Copy_u8PinNum);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN8 &&
						 Copy_u8PinNum <= DIO_u8_PIN15))
				{
					SET_BIT(DIO_u8_PORTB,Copy_u8PinNum-EIGHT);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN16 &&
						 Copy_u8PinNum <= DIO_u8_PIN23))
				{
					SET_BIT(DIO_u8_PORTC,Copy_u8PinNum-SIXTEEN);
				}
				else if((Copy_u8PinNum >= DIO_u8_PIN23 &&
						 Copy_u8PinNum <= DIO_u8_PIN31))
				{
					SET_BIT(DIO_u8_PORTD,Copy_u8PinNum-TWENTY_FOUR);
				}
				break;

			default:
				Local_u8Error = STD_u8_ERROR;
				break;

		}
	}
	else
	{
		Local_u8Error = STD_u8_ERROR;
	}

	return Local_u8Error;
}

u8 DIO_u8GetPinValue(u8 Copy_u8PinNum, u8 *Copy_Pu8PinValueAddress)
{
	u8 Local_u8Error;
	u8 Local_u8Value;

	if((Copy_u8PinNum < DIO_u8_MAX_PIN_NUM) && (Copy_Pu8PinValueAddress != NULL))
	{
		if((Copy_u8PinNum >= DIO_u8_PIN0 &&
			Copy_u8PinNum <= DIO_u8_PIN7))
		{
			Local_u8Value = GET_BIT(DIO_u8_PINA,Copy_u8PinNum);

			switch(Local_u8Value)
			{
				case DIO_PIN_INIT_VAL_LOW:
					*Copy_Pu8PinValueAddress = DIO_u8_PIN_LOW;
					break;

				case DIO_PIN_INIT_VAL_HIGH:
					*Copy_Pu8PinValueAddress = DIO_u8_PIN_HIGH;
					break;
			}
		}
		else if((Copy_u8PinNum >= DIO_u8_PIN8 &&
				 Copy_u8PinNum <= DIO_u8_PIN15))
		{
			Local_u8Value = GET_BIT(DIO_u8_PINB,Copy_u8PinNum-EIGHT);

			switch(Local_u8Value)
			{
				case DIO_PIN_INIT_VAL_LOW:
					*Copy_Pu8PinValueAddress = DIO_u8_PIN_LOW;
					break;

				case DIO_PIN_INIT_VAL_HIGH:
					*Copy_Pu8PinValueAddress = DIO_u8_PIN_HIGH;
					break;
			}
		}
		else if((Copy_u8PinNum >= DIO_u8_PIN16 &&
				 Copy_u8PinNum <= DIO_u8_PIN23))
		{
			Local_u8Value = GET_BIT(DIO_u8_PINC,Copy_u8PinNum-SIXTEEN);

			switch(Local_u8Value)
			{
				case DIO_PIN_INIT_VAL_LOW:
					*Copy_Pu8PinValueAddress = DIO_u8_PIN_LOW;
					break;

				case DIO_PIN_INIT_VAL_HIGH:
					*Copy_Pu8PinValueAddress = DIO_u8_PIN_HIGH;
					break;
			}
		}
		else if((Copy_u8PinNum >= DIO_u8_PIN23 &&
				 Copy_u8PinNum <= DIO_u8_PIN31))
		{
			Local_u8Value = GET_BIT(DIO_u8_PIND,Copy_u8PinNum-TWENTY_FOUR);

			switch(Local_u8Value)
			{
				case DIO_PIN_INIT_VAL_LOW:
					*Copy_Pu8PinValueAddress = DIO_u8_PIN_LOW;
					break;

				case DIO_PIN_INIT_VAL_HIGH:
					*Copy_Pu8PinValueAddress = DIO_u8_PIN_HIGH;
					break;
			}
		}
	}
	else
	{
		Local_u8Error = STD_u8_ERROR;
	}

	return Local_u8Error;
}



u8 DIO_u8SetPortDirection(u8 Copy_u8PortNum, u8 Copy_u8PortDirection)
{
	u8 Local_u8Error;

	if(Copy_u8PortNum < DIO_u8_MAX_PORT_NUM)
	{
		switch(Copy_u8PortNum)
		{
			case DIO_u8_PORT0:
				ASG_BYTE(DIO_u8_DDRA,Copy_u8PortDirection);
				break;

			case DIO_u8_PORT1:
				ASG_BYTE(DIO_u8_DDRB,Copy_u8PortDirection);
				break;

			case DIO_u8_PORT2:
				ASG_BYTE(DIO_u8_DDRC,Copy_u8PortDirection);
				break;

			case DIO_u8_PORT3:
				ASG_BYTE(DIO_u8_DDRD,Copy_u8PortDirection);
				break;

		}
		Local_u8Error = STD_u8_OK;
	}
	else
	{
		Local_u8Error = STD_u8_ERROR;
	}

	return Local_u8Error;
}

u8 DIO_u8SetPortValue(u8 Copy_u8PortNum, u8 Copy_u8PortValue)
{
	u8 Local_u8Error;

	if(Copy_u8PortNum < DIO_u8_MAX_PORT_NUM)
	{
		switch(Copy_u8PortNum)
		{
			case DIO_u8_PORT0:
				ASG_BYTE(DIO_u8_PORTA,Copy_u8PortValue);
				break;

			case DIO_u8_PORT1:
				ASG_BYTE(DIO_u8_PORTB,Copy_u8PortValue);
				break;

			case DIO_u8_PORT2:
				ASG_BYTE(DIO_u8_PORTC,Copy_u8PortValue);
				break;

			case DIO_u8_PORT3:
				ASG_BYTE(DIO_u8_PORTD,Copy_u8PortValue);
				break;

		}
		Local_u8Error = STD_u8_OK;
	}
	else
	{
		Local_u8Error = STD_u8_ERROR;
	}

	return Local_u8Error;
}

u8 DIO_u8GetPortValue(u8 Copy_u8PortNum, u8 *Copy_Pu8PortValueAddress)
{
	u8 Local_u8Error;

	if((Copy_u8PortNum < DIO_u8_MAX_PORT_NUM) && (Copy_Pu8PortValueAddress != NULL))
	{
		switch(Copy_u8PortNum)
		{
			case DIO_u8_PORT0:
				*Copy_Pu8PortValueAddress = GET_BYTE(DIO_u8_PINA);
				break;

			case DIO_u8_PORT1:
				*Copy_Pu8PortValueAddress = GET_BYTE(DIO_u8_PINB);
				break;

			case DIO_u8_PORT2:
				*Copy_Pu8PortValueAddress = GET_BYTE(DIO_u8_PINC);
				break;

			case DIO_u8_PORT3:
				*Copy_Pu8PortValueAddress = GET_BYTE(DIO_u8_PIND);
				break;

		}
	}
	else
	{
		Local_u8Error = STD_u8_ERROR;
	}

	return Local_u8Error;

}

