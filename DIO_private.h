/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          February 7th, 2019                  */
/*   Version :          V03                                 */
/*                                                          */
/* ******************************************************** */


/* Preprocessor Guard                                       */
#ifndef DIO_PRIVATE_H_
#define DIO_PRIVATE_H_



#define EIGHT							8
#define SIXTEEN							16
#define TWENTY_FOUR						24



#define DIO_PIN_INIT_DIR_INPUT			0
#define DIO_PIN_INIT_DIR_OUTPUT			1

#define DIO_PIN_INIT_VAL_LOW			0
#define DIO_PIN_INIT_VAL_HIGH			1



#define DIO_u8_NUMBER_PINS_IN_PORT 	(u8)8

#define DIO_u8_MAX_PIN_NUM 			(u8)32

#define DIO_u8_MAX_PORT_NUM			(u8)4



/* Defining registers of PORT A */
#define DIO_u8_PORTA		((Register*)0x3B)->ByteAccess
#define DIO_u8_DDRA 		((Register*)0x3A)->ByteAccess
#define DIO_u8_PINA			((Register*)0x39)->ByteAccess

/* Defining pins of PORTA */
#define DIO_u8_PORTA_PIN0 ((Register*)0x3B)->BitAccess.Bit0
#define DIO_u8_PORTA_PIN1 ((Register*)0x3B)->BitAccess.Bit1
#define DIO_u8_PORTA_PIN2 ((Register*)0x3B)->BitAccess.Bit2
#define DIO_u8_PORTA_PIN3 ((Register*)0x3B)->BitAccess.Bit3
#define DIO_u8_PORTA_PIN4 ((Register*)0x3B)->BitAccess.Bit4
#define DIO_u8_PORTA_PIN5 ((Register*)0x3B)->BitAccess.Bit5
#define DIO_u8_PORTA_PIN6 ((Register*)0x3B)->BitAccess.Bit6
#define DIO_u8_PORTA_PIN7 ((Register*)0x3B)->BitAccess.Bit7

/* Defining pins of DDRA */
#define DIO_u8_DDRA_PIN0 ((Register*)0x3A)->BitAccess.Bit0
#define DIO_u8_DDRA_PIN1 ((Register*)0x3A)->BitAccess.Bit1
#define DIO_u8_DDRA_PIN2 ((Register*)0x3A)->BitAccess.Bit2
#define DIO_u8_DDRA_PIN3 ((Register*)0x3A)->BitAccess.Bit3
#define DIO_u8_DDRA_PIN4 ((Register*)0x3A)->BitAccess.Bit4
#define DIO_u8_DDRA_PIN5 ((Register*)0x3A)->BitAccess.Bit5
#define DIO_u8_DDRA_PIN6 ((Register*)0x3A)->BitAccess.Bit6
#define DIO_u8_DDRA_PIN7 ((Register*)0x3A)->BitAccess.Bit7

/* Defining pins of PINA */
#define DIO_u8_PINA_PIN0 ((Register*)0x39)->BitAccess.Bit0
#define DIO_u8_PINA_PIN1 ((Register*)0x39)->BitAccess.Bit1
#define DIO_u8_PINA_PIN2 ((Register*)0x39)->BitAccess.Bit2
#define DIO_u8_PINA_PIN3 ((Register*)0x39)->BitAccess.Bit3
#define DIO_u8_PINA_PIN4 ((Register*)0x39)->BitAccess.Bit4
#define DIO_u8_PINA_PIN5 ((Register*)0x39)->BitAccess.Bit5
#define DIO_u8_PINA_PIN6 ((Register*)0x39)->BitAccess.Bit6
#define DIO_u8_PINA_PIN7 ((Register*)0x39)->BitAccess.Bit7




/* Defining registers of PORT B */
#define DIO_u8_PORTB		((Register*)0x38)->ByteAccess
#define DIO_u8_DDRB			((Register*)0x37)->ByteAccess
#define DIO_u8_PINB			((Register*)0x36)->ByteAccess

/* Defining pins of PORTB */
#define DIO_u8_PORTB_PIN0 ((Register*)0x38)->BitAccess.Bit0
#define DIO_u8_PORTB_PIN1 ((Register*)0x38)->BitAccess.Bit1
#define DIO_u8_PORTB_PIN2 ((Register*)0x38)->BitAccess.Bit2
#define DIO_u8_PORTB_PIN3 ((Register*)0x38)->BitAccess.Bit3
#define DIO_u8_PORTB_PIN4 ((Register*)0x38)->BitAccess.Bit4
#define DIO_u8_PORTB_PIN5 ((Register*)0x38)->BitAccess.Bit5
#define DIO_u8_PORTB_PIN6 ((Register*)0x38)->BitAccess.Bit6
#define DIO_u8_PORTB_PIN7 ((Register*)0x38)->BitAccess.Bit7

/* Defining pins of DDRB */
#define DIO_u8_DDRB_PIN0 ((Register*)0x37)->BitAccess.Bit0
#define DIO_u8_DDRB_PIN1 ((Register*)0x37)->BitAccess.Bit1
#define DIO_u8_DDRB_PIN2 ((Register*)0x37)->BitAccess.Bit2
#define DIO_u8_DDRB_PIN3 ((Register*)0x37)->BitAccess.Bit3
#define DIO_u8_DDRB_PIN4 ((Register*)0x37)->BitAccess.Bit4
#define DIO_u8_DDRB_PIN5 ((Register*)0x37)->BitAccess.Bit5
#define DIO_u8_DDRB_PIN6 ((Register*)0x37)->BitAccess.Bit6
#define DIO_u8_DDRB_PIN7 ((Register*)0x37)->BitAccess.Bit7

/* Defining pins of PINB */
#define DIO_u8_PINB_PIN0 ((Register*)0x36)->BitAccess.Bit0
#define DIO_u8_PINB_PIN1 ((Register*)0x36)->BitAccess.Bit1
#define DIO_u8_PINB_PIN2 ((Register*)0x36)->BitAccess.Bit2
#define DIO_u8_PINB_PIN3 ((Register*)0x36)->BitAccess.Bit3
#define DIO_u8_PINB_PIN4 ((Register*)0x36)->BitAccess.Bit4
#define DIO_u8_PINB_PIN5 ((Register*)0x36)->BitAccess.Bit5
#define DIO_u8_PINB_PIN6 ((Register*)0x36)->BitAccess.Bit6
#define DIO_u8_PINB_PIN7 ((Register*)0x36)->BitAccess.Bit7





/* Defining registers of PORT C */
#define DIO_u8_PORTC		((Register*)0x35)->ByteAccess
#define DIO_u8_DDRC			((Register*)0x34)->ByteAccess
#define DIO_u8_PINC			((Register*)0x33)->ByteAccess

/* Defining pins of PORTC */
#define DIO_u8_PORTC_PIN0 ((Register*)0x35)->BitAccess.Bit0
#define DIO_u8_PORTC_PIN1 ((Register*)0x35)->BitAccess.Bit1
#define DIO_u8_PORTC_PIN2 ((Register*)0x35)->BitAccess.Bit2
#define DIO_u8_PORTC_PIN3 ((Register*)0x35)->BitAccess.Bit3
#define DIO_u8_PORTC_PIN4 ((Register*)0x35)->BitAccess.Bit4
#define DIO_u8_PORTC_PIN5 ((Register*)0x35)->BitAccess.Bit5
#define DIO_u8_PORTC_PIN6 ((Register*)0x35)->BitAccess.Bit6
#define DIO_u8_PORTC_PIN7 ((Register*)0x35)->BitAccess.Bit7

/* Defining pins of DDRC */
#define DIO_u8_DDRC_PIN0 ((Register*)0x34)->BitAccess.Bit0
#define DIO_u8_DDRC_PIN1 ((Register*)0x34)->BitAccess.Bit1
#define DIO_u8_DDRC_PIN2 ((Register*)0x34)->BitAccess.Bit2
#define DIO_u8_DDRC_PIN3 ((Register*)0x34)->BitAccess.Bit3
#define DIO_u8_DDRC_PIN4 ((Register*)0x34)->BitAccess.Bit4
#define DIO_u8_DDRC_PIN5 ((Register*)0x34)->BitAccess.Bit5
#define DIO_u8_DDRC_PIN6 ((Register*)0x34)->BitAccess.Bit6
#define DIO_u8_DDRC_PIN7 ((Register*)0x34)->BitAccess.Bit7

/* Defining pins of PINC */
#define DIO_u8_PINC_PIN0 ((Register*)0x33)->BitAccess.Bit0
#define DIO_u8_PINC_PIN1 ((Register*)0x33)->BitAccess.Bit1
#define DIO_u8_PINC_PIN2 ((Register*)0x33)->BitAccess.Bit2
#define DIO_u8_PINC_PIN3 ((Register*)0x33)->BitAccess.Bit3
#define DIO_u8_PINC_PIN4 ((Register*)0x33)->BitAccess.Bit4
#define DIO_u8_PINC_PIN5 ((Register*)0x33)->BitAccess.Bit5
#define DIO_u8_PINC_PIN6 ((Register*)0x33)->BitAccess.Bit6
#define DIO_u8_PINC_PIN7 ((Register*)0x33)->BitAccess.Bit7





/* Defining registers of PORT D */
#define DIO_u8_PORTD		((Register*)0x32)->ByteAccess
#define DIO_u8_DDRD			((Register*)0x31)->ByteAccess
#define DIO_u8_PIND			((Register*)0x30)->ByteAccess

/* Defining pins of PORTD */
#define DIO_u8_PORTD_PIN0 ((Register*)0x32)->BitAccess.Bit0
#define DIO_u8_PORTD_PIN1 ((Register*)0x32)->BitAccess.Bit1
#define DIO_u8_PORTD_PIN2 ((Register*)0x32)->BitAccess.Bit2
#define DIO_u8_PORTD_PIN3 ((Register*)0x32)->BitAccess.Bit3
#define DIO_u8_PORTD_PIN4 ((Register*)0x32)->BitAccess.Bit4
#define DIO_u8_PORTD_PIN5 ((Register*)0x32)->BitAccess.Bit5
#define DIO_u8_PORTD_PIN6 ((Register*)0x32)->BitAccess.Bit6
#define DIO_u8_PORTD_PIN7 ((Register*)0x32)->BitAccess.Bit7

/* Defining pins of DDRD */
#define DIO_u8_DDRD_PIN0 ((Register*)0x31)->BitAccess.Bit0
#define DIO_u8_DDRD_PIN1 ((Register*)0x31)->BitAccess.Bit1
#define DIO_u8_DDRD_PIN2 ((Register*)0x31)->BitAccess.Bit2
#define DIO_u8_DDRD_PIN3 ((Register*)0x31)->BitAccess.Bit3
#define DIO_u8_DDRD_PIN4 ((Register*)0x31)->BitAccess.Bit4
#define DIO_u8_DDRD_PIN5 ((Register*)0x31)->BitAccess.Bit5
#define DIO_u8_DDRD_PIN6 ((Register*)0x31)->BitAccess.Bit6
#define DIO_u8_DDRD_PIN7 ((Register*)0x31)->BitAccess.Bit7

/* Defining pins of PIND */
#define DIO_u8_PIND_PIN0 ((Register*)0x30)->BitAccess.Bit0
#define DIO_u8_PIND_PIN1 ((Register*)0x30)->BitAccess.Bit1
#define DIO_u8_PIND_PIN2 ((Register*)0x30)->BitAccess.Bit2
#define DIO_u8_PIND_PIN3 ((Register*)0x30)->BitAccess.Bit3
#define DIO_u8_PIND_PIN4 ((Register*)0x30)->BitAccess.Bit4
#define DIO_u8_PIND_PIN5 ((Register*)0x30)->BitAccess.Bit5
#define DIO_u8_PIND_PIN6 ((Register*)0x30)->BitAccess.Bit6
#define DIO_u8_PIND_PIN7 ((Register*)0x30)->BitAccess.Bit7



#endif /* DIO_PRIVATE_H_ */
